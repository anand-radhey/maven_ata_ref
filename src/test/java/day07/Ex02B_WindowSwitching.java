package day07;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Ex02B_WindowSwitching {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		driver.get("https://ataevents.org/");
		
		
		//*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[1]/div/div/div/div/div/figure/a/img
		//*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[2]/div/div/div/div/div/figure/a/img
		//*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[3]/div/div/div/div/div/figure/a/img
		//*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[*]/div/div/div/div/div/figure/a/img
		
		By byvar = By.xpath("//*[@id=\"post-18\"]/div/div/div/div/div/section[3]/div/div/div[*]/div/div/div/div/div/figure/a/img");
		List<WebElement>  lst =  driver.findElements(byvar);
		
		int size = lst.size();
		
		System.out.println("No. of WebElements in the List : "+ size);
		System.out.println(driver.getTitle());
		
		for(int i=0; i < size ; i++) {	
			WebElement we = lst.get(i);
			we.click();
			System.out.println(i + " ::  Title :: " + driver.getTitle());	
		}
		
		Set<String> winset   =     driver.getWindowHandles();
		
		for( String ele   :  winset) {
			System.out.println("Window Handle :: "+ ele);	
			driver.switchTo().window(ele);
			System.out.println("The Title is : "+ driver.getTitle());	
		}
		
		
		for( String ele   :  winset) {
			System.out.println("Window Handle :: "+ ele);	
			driver.switchTo().window(ele);
			String actTitle = driver.getTitle();
			System.out.println("The Title is : "+ driver.getTitle());	
			if(actTitle.contains("CP-WST")) {
				break;
				
			}
	
			
		}
		
		//*[@id="event_div"]/div/div/div[1]
		
		By byvar2 = By.xpath("//*[@id=\"event_div\"]/div/div/div[1]");
		WebElement weDates = driver.findElement(byvar2);
		String str = weDates.getText();
		System.out.println("Text Associated ::: " + str);
		if(str.contains("21st May 2021")) {
			System.out.println("Test Passed ");
		} else {
			System.out.println("Test Failed ");
		}
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//driver.close();
		driver.quit();
		
	}

}
