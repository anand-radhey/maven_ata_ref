package day07;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Ex02_WikiSearch_JU {
	WebDriver driver;
	

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		
		driver.manage().window().maximize();	
	}
	
	@Test
	public void test() {
		
		driver.get("https://www.wikipedia.org/");
		By byvar = By.partialLinkText("English");
		WebElement link = driver.findElement(byvar);
		link.click();
		
		By bybox = By.id("searchInput");
		WebElement wesearch = driver.findElement(bybox);
		
		wesearch.sendKeys("Selenium");
		
		By byglass = By.name("go");
		WebElement weglass = driver.findElement(byglass);
		
		weglass.click();
		
		String title = driver.getTitle();
		String expTitle = "Selenium - Wikipedia";
		
		if(title.equals(expTitle)) {
			System.out.println("Title Matches");
		} else {
			System.out.println("Title NOT Matches");
		}
	
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		
	}

}
