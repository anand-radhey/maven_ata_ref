package day07;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Ex03_AnnaUniversity {

	public static void main(String[] args) {
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		driver.get("https://www.annauniv.edu/department/index.php");
		
		
		String filename = "src\\test\\resources\\screenshots\\Anna_page1_02.png";
		
		utils.HelperFunctions.captureScreenShot(driver, filename);
	
		//*[@id="link3"]/strong        Civil Engg
		WebElement wecivil = driver.findElement(By.xpath("//*[@id=\"link3\"]/strong "));
		
		//*[@id="menuItemText33"]       IOM
		By byvar2 = By.xpath("//*[@id=\"menuItemText33\"]");
		WebElement weiom = driver.findElement(byvar2);
		
		Actions act = new Actions(driver);
		act.moveToElement(wecivil).moveToElement(weiom).click().perform();
		
		System.out.println(driver.getTitle());
		
		filename = "src\\test\\resources\\screenshots\\Anna_page2_02.png";
		
		utils.HelperFunctions.captureScreenShot(driver, filename);
		
		driver.quit();
		
	}

}
