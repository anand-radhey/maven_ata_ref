package day08;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Ex03_AnnaUniversity_JU {
	WebDriver driver;
	

	@Before
	public void setUp() throws Exception {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome");
		
		
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void test() {
driver.get("https://www.annauniv.edu/department/index1.php");
		
		
		String filename = "src\\test\\resources\\screenshots\\Anna_page1_02.png";
		
		utils.HelperFunctions.captureScreenShot(driver, filename);
	
		//*[@id="link3"]/strong        Civil Engg
		WebElement wecivil = driver.findElement(By.xpath("//*[@id=\"link3\"]/strong "));
		
		//*[@id="menuItemText33"]       IOM
		By byvar2 = By.xpath("//*[@id=\"menuItemText33\"]");
		WebElement weiom = driver.findElement(byvar2);
		
		Actions act = new Actions(driver);
		act.moveToElement(wecivil).moveToElement(weiom).click().perform();
		
		System.out.println(driver.getTitle());
		
		filename = "src\\test\\resources\\screenshots\\Anna_page2_02.png";
		
		utils.HelperFunctions.captureScreenShot(driver, filename);
	}

}
