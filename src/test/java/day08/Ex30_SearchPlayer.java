package day08;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

public class Ex30_SearchPlayer {

	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException, IOException {
		String filename = "C:\\ZVA\\CPSAT\\maven_ATA_Ref_ORG\\src\\test\\resources\\data\\Cricket_Score_India_Eng_SA.xlsx";

		String[] sheets = {"India", "England", "SA"};

		// Jos Buttler  	EN05	451
		String expName = "Jos Buttler  ".trim();
		String expID  = "EN05".trim();
		int expScore = 451;

		List<Player>  playerList = new ArrayList<Player>();
		boolean matchFound = false;

		for( int k=0; k < sheets.length ; k++) {	
			String sheetname = sheets[k];	
			String[][]  datafromxl = utils.XLDataReaders.getExcelData(filename, sheetname);

			int nrows = datafromxl.length;
			int ncols = datafromxl[0].length;
			for( int i=0; i < nrows; i++  ) {
				String name = datafromxl[i][0];
				String id = datafromxl[i][1];
				String score = datafromxl[i][2];
				int iscore = Integer.parseInt(score);

				Player pl = new Player(name,id, iscore);
				playerList.add(pl);
			}
		}

		System.out.println(playerList);

		for( Player p   : playerList) {
			if ( expName.equals(p.getName().trim())   &&  expID.equals(p.getId().trim())  && expScore == p.getScore()   ) {
				matchFound = true;
				break;	
			}	
		}

		if(matchFound) {
			System.out.println("Match Found for "+ expName);
		} else {
			System.out.println("Could not find Match for "+ expName);
		}



	}

}
