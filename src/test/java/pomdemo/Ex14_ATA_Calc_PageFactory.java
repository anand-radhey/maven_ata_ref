package pomdemo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Ex14_ATA_Calc_PageFactory {
	WebDriver driver;
	
	@FindBy(xpath= "//*[@id=\"ID_nameField1\"]")
	WebElement weField1; 
	
	@FindBy(xpath="//*[@id=\"ID_nameField2\"]")
	WebElement weField2;
	
	@FindBy(xpath="//*[@id=\"gwt-uid-2\"]")
	WebElement weMul;
	
	@FindBy(xpath="//*[@id=\"gwt-uid-1\"]")
	WebElement weAdd;
	
	@FindBy(xpath="//*[@id=\"ID_calculator\"]")
	WebElement weCalc;
	
	@FindBy(xpath="//*[@id=\"ID_nameField3\"]")
	WebElement weRes;
	
	
	public Ex14_ATA_Calc_PageFactory(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;	
	}
	
	
	public String	multiply(String in1, String in2){
		String actRes = "";

//		WebElement weField1 = driver.findElement(byField1);
//		WebElement weField2 = driver.findElement(byField2);
//		WebElement weMul = driver.findElement(byMul);
//		WebElement weAdd = driver.findElement(byMul);
//		WebElement weCalc = driver.findElement(byCalc);
//		WebElement weRes = driver.findElement(byRes);
//		
		weField1.clear();
		weField1.sendKeys( in1);
		weField2.clear();
		weField2.sendKeys(in2);
		
		weMul.click();
		weCalc.click();
		
		actRes =  weRes.getAttribute("value");   //weRes.getText();
		return actRes;
	}
	
	public String	addition(String in1, String in2){
		String actRes = "";

		weField1.clear();
		weField1.sendKeys( in1);
		weField2.clear();
		weField2.sendKeys(in2);
		
		weAdd.click();
		weCalc.click();
		
		actRes =  weRes.getAttribute("value");   //weRes.getText();
		return actRes;
	}

	
	

}
