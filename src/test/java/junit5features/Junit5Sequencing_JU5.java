package junit5features;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.condition.JRE.JAVA_10;
import static org.junit.jupiter.api.condition.JRE.JAVA_8;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledOnOs;
import org.junit.jupiter.api.condition.EnabledOnJre;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;

class Junit5Sequencing_JU5 {

	/*@BeforeEach
	void setUp() throws Exception {
		System.out.println("Inside setup()");
	}

	@AfterEach
	void tearDown() throws Exception {
		System.out.println("Inside tearDown()");
	}
*/
	
	//@DisplayName(value = "Payments")
	
	@Test
	void first() {
		System.out.println("Inside first()");
	}
	
	//@EnabledOnJre({  JAVA_10 })
	//@DisplayName(value = "Logistics")
	@Test
	void second() {
		System.out.println("Inside second()");
	}
	
	
	//@DisplayName(value = "Display")
	@Test
	void third() {
		System.out.println("Inside third()");
	}
	
	 // @EnabledOnJre({ JAVA_8, JAVA_10 })
	    @EnabledOnOs(OS.WINDOWS)
	@Test
	void fourth() {
		System.out.println("Inside fourth()");
	}
	
	//@Order(4)
	@Test
	void fifth() {
		System.out.println("Inside fifth()");
	}

}
