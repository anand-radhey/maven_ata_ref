package junit5features;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;

class Ex_SampleDemo_JU5 {

	
	void first() {
		System.out.println("Inside first()");
	}
	
	
	@Test
	void second() {
		System.out.println("Inside second()");
	}
	
	
	
	@Test
	void third() {
		System.out.println("Inside third()");
	}
	
	
	@Test
	void fourth() {
		System.out.println("Inside fourth()");
	}
	
	
	@Test
	void fifth() {
		System.out.println("Inside fifth()");
	}

}
