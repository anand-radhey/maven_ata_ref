package junit5features;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.DisabledOnOs;
import org.junit.jupiter.api.condition.EnabledOnJre;
import org.junit.jupiter.api.condition.OS;

import static org.junit.jupiter.api.condition.JRE.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class JU5_Sequencing {

    @BeforeEach
    public void setUP(){
        System.out.println("Inside Setup()");
    }

    @AfterEach
    public void tearDown(){
        System.out.println("Inside tearDown()");
    }

    @Order(5)
    //@DisabledOnOs(OS.WINDOWS)
    @DisplayName("Method_1")
    @Test
    public void first(){
        System.out.println("Inside first()");
    }

    @Order(4)
    //@Disabled("Would be enabling after Fixing Bug#254")
    @Test
    public void second(){
        System.out.println("Inside second()");
    }

    @Order(3)
    @EnabledOnJre({ JAVA_8, JAVA_10 })
    @DisabledOnOs(OS.LINUX)
    @Test
    public void third(){
        System.out.println("Inside third()");
    }

    @Order(2)
    @Test
    public void fourth(){
        System.out.println("Inside fourth()");
    }

    @Order(1)
    @Test
    public void fifth(){
        System.out.println("Inside fifth()");
    }










}
