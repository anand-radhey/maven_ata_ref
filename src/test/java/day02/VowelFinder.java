package day02;

public class VowelFinder {
	/*
	Ex06 : Given a char, determine if it is a vowel or not?
*/
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n = 12;
		char ch_in = 'E';
		
		//  a  e i o u
		
		//Wrapper classes
		char ch = Character.toLowerCase(ch_in);
		
		if(ch == 'a'  ||    ch == 'e' ||    ch == 'i' ||    ch == 'o' ||    ch == 'u'  ) {
			System.out.println(ch_in  + " the Given character is a vowel");
		} else {
			System.out.println(ch_in  + " the Given character is NOT a vowel");
		}
		

	}

}
