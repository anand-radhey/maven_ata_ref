package sharedbyteam;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.chrome.ChromeDriver;

public class Wiki_Navigation {

	public static void main(String[] args) {

		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");

		WebDriver driver= new ChromeDriver();

		driver.manage().window().maximize();

		driver.get("https://www.wikipedia.org/");
		
		System.out.println("Page-1 title : "+ driver.getTitle());

		By byvar= By.partialLinkText("English");

		WebElement link= driver.findElement(byvar);

		link.click();
		
		System.out.println("Page-2 title : "+ driver.getTitle());

		By byvar2= By.name("search");

		WebElement search= driver.findElement(byvar2);

		search.sendKeys("Selenium");

		By byglass= By.name("go");

		WebElement glass= driver.findElement(byglass);

		glass.click();

		System.out.println("Page-3 title : "+ driver.getTitle());

		
		driver.navigate().back();
		
		
		String pageTitle= driver.getTitle();

		System.out.println("After Navigation back  The title of the page is: " +pageTitle);
		
		driver.navigate().forward();
		

		String pageTitle2= driver.getTitle();

		System.out.println("After Forward Navigation  The title of the page is: " +pageTitle2);

		try

		{
			Thread.sleep(5000);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}

		driver.close();

	}



}