package day09;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Ex07_IMDB_DP_NG {
WebDriver driver;
	
	@BeforeTest
	  public void beforeTest() {
		System.out.println("Inside beforeTest() ");
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome");
	  }
	
	 @DataProvider
	  public Object[][] dp() throws EncryptedDocumentException, InvalidFormatException, IOException {
		 
		 String filename = "src\\test\\resources\\data\\imdbdata.xlsx";
		 String sheetname = "data";
		 
		 String[][] moviedata = utils.XLDataReaders.getExcelData(filename, sheetname);
		 
		 return moviedata;
		 
		 
		 /*String[][] moviedata = {
				 {"baazigar", "Mastan", "Shah Rukh Khan"},
				 {"Raazi", "Meghna Gulzar", "Vicky Kaushal"},
				 {"102 Not Out", "Umesh Shukla", "Rishi Kapoor"},
				 {"Uri", "Aditya Dhar", "Vicky Kaushal"}
		 };
		 return moviedata;
		 *
		 */
		 
		
		 
		 
	   /* return new Object[][] {
	      new Object[] { 1, "a" },
	      new Object[] { 2, "b" },
	    };*/
	  }
	
	
	
  @Test(dataProvider = "dp")
  public void movieTest(String v1, String v2, String v3) {
	  System.out.println("Inside MovieTest() ");
	  System.out.println("v1 : "+ v1);
	  System.out.println("v2 : "+ v2);
	  System.out.println("v3 : "+ v3);
	  String movie = v1;
	  String expDir = v2; //"Mastan";
	  String expStar = v3; // "Shah Rukh Khan";
	  
	  driver.get("https://www.imdb.com/");  //Page 1
	  
	//*[@id="suggestion-search"]    searchbox
	//input[@name='q']              searchbox
	  //By byvarsearch = By.xpath("//*[@id=\"suggestion-search\"]");
	  
	  By byvarsearch = By.id("suggestion-search");
	  WebElement searchbox = driver.findElement(byvarsearch);
	  
	  searchbox.sendKeys(movie);
	  
	//*[@id="suggestion-search-button"]
	  
	  By byvarglass = By.xpath("//*[@id=\"suggestion-search-button\"]");
	  WebElement weglass = driver.findElement(byvarglass);
	  weglass.click();
	  
	  // This displays page 2

	//*[@id="main"]/div/div[2]/table/tbody/tr[1]/td[2]/a   
	  
	  driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a")).click();
	  
	  // This displays page 3
	  
	//*[contains( text(), 'Directors'   )]/following-sibling::*      Director's siblings nodes
	  
	  List<WebElement>   listDir = driver.findElements(By.xpath("//*[contains( text(), 'Director'   )]/following-sibling::*"));
	  boolean foundDir = false;
	  for( WebElement dir     :  listDir) {
		  String dirName = dir.getText();
		  if(dirName.contains(expDir)) {
			  foundDir = true;
			  System.out.println("Director Found " + expDir);
			  break; 
		  }  
	  }
	  
	//*[contains(text(), 'Star')]/following-sibling::*
	  
	  List<WebElement>   listStar = driver.findElements(By.xpath("//*[contains( text(), 'Star'   )]/following-sibling::*"));
	  boolean foundStar = false;
	  for( WebElement star     :  listStar) {
		  String starName = star.getText();
		  if(starName.contains(expStar)) {
			  foundStar = true;
			  System.out.println("Star Found " + expStar);
			  break; 
		  }  
	  }
	  
	  
	  
	  
	  Assert.assertEquals(  (foundStar && foundDir)  , true, "Either Dir or Star or Both are mismatching ");
	  //if (foundStar && foundDir)
	  
	  
	  System.out.println("Exit from  MovieTest() ");
  }

 
 

  @AfterTest
  public void afterTest() {
	  System.out.println("Inside afterTest() ");
	  driver.quit();
  }

}
