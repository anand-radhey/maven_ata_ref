package day09;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class Exo4_Alerts_NG {
	WebDriver driver;
	
	@BeforeTest
	  public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		
	  }
	
	
  @Test
  public void myowntestmethod() {
	  String expResult = "You successfully clicked an alert";
	  WebDriverWait waitobj = new WebDriverWait(driver, 10);  // configuring wait object
	  
	  driver.get("https://the-internet.herokuapp.com/javascript_alerts");
	  
	
	  
	  By byvar1 = By.xpath("//*[@id=\"content\"]/div/ul/li[1]/button");
	  WebElement webutton1 = driver.findElement(byvar1);
	  webutton1.click();
	  
	 // Thread.sleep(millis);
	  
	 // driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  
	 // Explicit wait 
	  
	 waitobj.until(ExpectedConditions.alertIsPresent());
	  
	  Alert alert = driver.switchTo().alert();
	  
	  alert.accept();
	  
	//*[@id="result"]   xpath for Result string
	  
	  WebElement weresult = driver.findElement(By.xpath("//*[@id=\"result\"]"));
	  
	  String actResult = weresult.getText();
	  if(actResult.equals(expResult)) {
		  System.out.println("Test Pass ");
	  } else {
		  System.out.println("Test Failed "); 
	  }
	  
	  
	  
	  
	  
	  
  }
  

  @AfterTest
  public void afterTest() {
	  
	  try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  driver.quit();
	  
  }

}
