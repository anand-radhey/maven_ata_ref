package day09;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Ex07_IMDB_NG {
	WebDriver driver;
	
	@BeforeTest
	  public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome");
	  }
	
  @Test
  public void movieTest() {
	  String expDir = "MastanBhai";
	  String expStar = "Shah Rukh Khan";
	  
	  driver.get("https://www.imdb.com/");  //Page 1
	  
	//*[@id="suggestion-search"]    searchbox
	//input[@name='q']              searchbox
	  //By byvarsearch = By.xpath("//*[@id=\"suggestion-search\"]");
	  
	  By byvarsearch = By.id("suggestion-search");
	  WebElement searchbox = driver.findElement(byvarsearch);
	  
	  searchbox.sendKeys("baazigar");
	  
	//*[@id="suggestion-search-button"]
	  
	  By byvarglass = By.xpath("//*[@id=\"suggestion-search-button\"]");
	  WebElement weglass = driver.findElement(byvarglass);
	  weglass.click();
	  
	  // This displays page 2

	//*[@id="main"]/div/div[2]/table/tbody/tr[1]/td[2]/a   
	  
	  driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a")).click();
	  
	  // This displays page 3
	  
	//*[contains( text(), 'Directors'   )]/following-sibling::*      Director's siblings nodes
	  
	  List<WebElement>   listDir = driver.findElements(By.xpath("//*[contains( text(), 'Director'   )]/following-sibling::*"));
	  boolean foundDir = false;
	  for( WebElement dir     :  listDir) {
		  String dirName = dir.getText();
		  if(dirName.contains(expDir)) {
			  foundDir = true;
			  System.out.println("Director Found " + expDir);
			  break; 
		  }  
	  }
	  
	//*[contains(text(), 'Star')]/following-sibling::*
	  
	  List<WebElement>   listStar = driver.findElements(By.xpath("//*[contains( text(), 'Star'   )]/following-sibling::*"));
	  boolean foundStar = false;
	  for( WebElement star     :  listStar) {
		  String starName = star.getText();
		  if(starName.contains(expStar)) {
			  foundStar = true;
			  System.out.println("Star Found " + expStar);
			  break; 
		  }  
	  }
	  
	  //Assert.assertEquals(  (foundStar && foundDir)  , true, "Either Dir or Star or Both are mismatching ");  
	  
  }
  

  @AfterTest
  public void afterTest() {
	  try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  driver.quit();
  }

}
