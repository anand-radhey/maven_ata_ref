package day06;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Ex06_SocialMedia {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("Chrome");
		
		driver.get("https://agiletestingalliance.org/");
		
	
		/*
		By byLI = By.xpath("/html/body/div[2]/div/div/section[2]/div/div/div/div/div/div[1]/div/div/div[1]/a");
		WebElement weLI = driver.findElement(byLI);
		String strLI = weLI.getAttribute("href");
		
		System.out.println(strLI);*/
		
		//	/html/body/div[2]/div/div/section[2]/div/div/div/div/div/div[1]/div/div/div[1]/a
		//  /html/body/div[2]/div/div/section[2]/div/div/div/div/div/div[1]/div/div/div[2]/a
		
		By byvar = By.xpath("/html/body/div[2]/div/div/section[2]/div/div/div/div/div/div[1]/div/div/div[*]/a");
		List<WebElement>   listlinks = driver.findElements(byvar);
		
		for( WebElement ele  : listlinks  ) {
			String str = ele.getAttribute("href");
			System.out.println(str);
		}
		

	}

}
