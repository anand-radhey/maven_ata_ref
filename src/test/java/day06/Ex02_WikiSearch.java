package day06;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Ex02_WikiSearch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.get("https://www.wikipedia.org/");
		
		//*[@id="js-link-box-en"]/strong
		
		//By byvar = By.xpath("//*[@id=\"js-link-box-en\"]/strong");
		By byvar = By.partialLinkText("English");
		
		WebElement link = driver.findElement(byvar);
		
		link.click();
		
		//*[@id="searchInput"]         searchbox xpath
		//   name="search"
		//    id="searchInput"
		
		//By bybox = By.xpath("//*[@id=\"searchInput\"] ");
		//By bybox = By.name("search");
		By bybox = By.id("searchInput");
		WebElement wesearch = driver.findElement(bybox);
		
		wesearch.sendKeys("Selenium");
		
		//    id="searchButton"
		//*[@id="searchButton"]     xpath for glass
		// name="go"
		
		By byglass = By.name("go");
		WebElement weglass = driver.findElement(byglass);
		
		weglass.click();
		
		String title = driver.getTitle();
		String expTitle = "Selenium - Wikipedia";
		
		if(title.equals(expTitle)) {
			System.out.println("Title Matches");
		} else {
			System.out.println("Title NOT Matches");
		}
		
		
		
		driver.quit();
		
		
		
		
		

	}

}
