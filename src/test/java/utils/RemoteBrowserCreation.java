package utils;

import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

public class RemoteBrowserCreation { 


	//hostName should include http://
	public static WebDriver createRemoteDriver(String browserName, String hostName, String port) throws MalformedURLException
	{
		WebDriver driver;
		browserName = browserName.toUpperCase();

		if (hostName == null) {
			hostName="http://localhost"; //assuming that hub is running on localhost
		}

		if (port == null) {
			port = "4444"; //default port of hub
		}

		//if ()
		// normally hub gives a message
		// like below
		// Clients should connect to http://192.168.0.111:4444/wd/hub
		// we are constructing a url which connects to our hub
		
		String strURL = hostName+":"+port+"/wd/hub";
		
		System.out.println("hostname " + hostName );
		System.out.println("port " + port );
		
		System.out.println("URL =" + strURL);
		
		switch (browserName) {

		case "CHROME":

			ChromeOptions optionsChrome = new ChromeOptions();
			optionsChrome.addArguments("test-type");
			optionsChrome.addArguments("start-maximized");
			optionsChrome.addArguments("--disable-popup-blocking");
			optionsChrome.addArguments("disable-extensions");
			optionsChrome.addArguments("--disable-notifications");
			
			//optionsChrome.setExperimentalOption("useAutomationExtension", false);
			driver = new RemoteWebDriver(new URL(strURL), optionsChrome);

			break;

		case "FIREFOX":
			
			FirefoxOptions optionsff = new FirefoxOptions();
			
			driver = new RemoteWebDriver(new URL(strURL), optionsff);
			break;
		default:
			//create chrome by default
			ChromeOptions optionsDefault = new ChromeOptions();
			optionsDefault.addArguments("test-type");
			optionsDefault.addArguments("start-maximized");
			optionsDefault.addArguments("--disable-popup-blocking");
			optionsDefault.addArguments("disable-extensions");
			//optionsDefault.setExperimentalOption("useAutomationExtension", false);
			optionsDefault.addArguments("--disable-notifications");
			driver = new RemoteWebDriver(new URL(strURL), optionsDefault);
			break;
		}

 
		return driver;
	} // end of createRemoteDriver(browserName,hostName,port)


}

