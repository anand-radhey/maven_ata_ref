package testngfeatures;

import org.testng.SkipException;
import org.testng.annotations.Test;

public class P1_Alpha {
	
	
	@Test(enabled = true)
	public void first() {
		System.out.println("Inside method first() ");
		
		
	}
	
	@Test(enabled = false)
	public void second() {
		System.out.println("Inside method second() ");
		
		
	}
	
	@Test
	public void third() {
		System.out.println("Inside method third() ");	
		throw new SkipException("Decided for Skipping - we are not ready for execution of this test.");
	}
	
	@Test(enabled = true)
	public void fourth() {
		System.out.println("Inside method fourth() ");
		
		
	}
	
	@Test(enabled = true)
	public void fifth() {
		System.out.println("Inside method fifth() ");
		
		
	}
	
	

}
