package testngfeatures;

import org.testng.SkipException;
import org.testng.annotations.Test;

public class P1_Alphabetic {
	
	
	@Test
	public void first() {
		System.out.println("Inside method first() ");
		
		
	}
	
	@Test
	public void second() {
		System.out.println("Inside method second() ");
		
		
	}
	
	@Test
	public void third() {
		System.out.println("Inside method third() ");	
		//throw new SkipException("Decided for Skipping - we are not ready for execution of this test.");
	}
	
	@Test
	public void fourth() {
		System.out.println("Inside method fourth() ");
		
		
	}
	
	@Test
	public void fifth() {
		System.out.println("Inside method fifth() ");
		
		
	}
	
	

}
