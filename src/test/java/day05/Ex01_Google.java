package day05;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Ex01_Google {

	public static void main(String[] args) {
		
		/*System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();*/
		/*
		System.setProperty("webdriver.gecko.driver", "src\\test\\resources\\drivers\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		*/
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		
		//driver.manage().window().maximize();
		
		

		driver.get("https://www.google.com/");
		
		String pageTitle = driver.getTitle();
		String expResult = "Google";
		
		System.out.println("The title of the page is "+ pageTitle);
		
		if(expResult.equals(pageTitle)) {
			System.out.println("Test is passed : Actual result and expected result matches");
		} else {
			System.out.println("Test is failed : Actual result and expected result Don't match");
		}
		//            /html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input
	/*	By byvar = By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input");
		
		*/
		
		By byvar = By.name("q");
		
		WebElement weinputbox = driver.findElement(byvar);
		
		weinputbox.sendKeys("CPSAT");
		
		///Keys.ENTER;
		
		weinputbox.sendKeys(Keys.ENTER);
		
		String pageTitle2 = driver.getTitle();
		System.out.println("The title of the page 2 is "+ pageTitle2);
		

		//String str

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		driver.close();
		//driver.quit();



	}

}
