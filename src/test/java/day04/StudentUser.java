package day04;

public class StudentUser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student st1 = new Student(100, "Anand");
		Student st2 = new Student(101, "Anshu");
		Student st3 = new Student();
		Student st4 = new Student(109);
		
		System.out.println("st1.id= " + st1.getId()  );    //st1.id);
		System.out.println("st1.name= " + st1.getName());
		
		System.out.println("st2.id= " + st2.getId());
		System.out.println("st2.name= " + st2.getName());
		
		System.out.println("st3.id= " + st3.getId());
		System.out.println("st3.name= " + st3.getName());
		
		System.out.println("st4.id= " + st4.getId());
		System.out.println("st4.name= " + st4.getName());
		

	}

}
