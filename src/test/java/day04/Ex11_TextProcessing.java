package day04;

public class Ex11_TextProcessing {
/*
	Ex11_TextProcessing
	Count following in a Given Text
	a. No. of numeric digit
	b. No. of vovels
	c. No. of consonents 

	Given Text is as below :
	__________________
	"1) The stories have been translated into nearly every language in the world 
	that has a script. 2) The story form appeals to children while the wisdom in 
	them attracts adults. 3) The Panchatantra collection represents the earliest
	 folk tale form in the world of literature. 4) - There are several versions of 
	 Panchatantra tales in circulation in the world but the one that is popular in 
	 India is the Sanskrit original of Vishnu Sharman."


	*/
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String instr = "1) The stories have been translated into nearly every language in the world that has a script. 2) The story form appeals to children while the wisdom in them attracts adults. 3) The Panchatantra collection represents the earliest folk tale form in the world of literature. 4) - There are several versions of Panchatantra tales in circulation in the world but the one that is popular in India is the Sanskrit original of Vishnu Sharman.";
		
		int len = instr.length();
		int ndigit = 0;
		int nvowel  = 0;
		int nconsonants = 0;
		
		for(int i = 0; i < len; i++) {
			char ch = instr.charAt(i);
			
			if (ch >= '0' &&   ch <= '9') {
				ndigit++;
				
			}
			
			
		}
		
		
		System.out.println(" No. of digits = "+ ndigit);
	}

}
