package day01;

public class PrintTable {
	
	// To print a table of given integer   7
	/*
	7  *  1  = 7
			7 * 2 = 14
			7 * 3 =21
			
			
			7 * 10 = 70
			*/

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		System.out.println("7 * 1 = 7 ");
		System.out.println("7 * 2 = 14 ");
		System.out.println("7 * 3 = 21 ");
		System.out.println("7 * 4 = 28 ");
		System.out.println("7 * 5 = 35 ");*/
		
		int n = 7;
		
		for( int i=1       ;   i <= 10       ;   i++  ) {
			
			System.out.println(  n   +  " * " + i  + " = " + ( n* i )  );
			
		}
		
		
		for ( int j = 25 ; j > 10  ; j = j - 5) {
			System.out.println(j);
		}
		
		
		

	}

}
