package day10;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;

public class Ex09_DropDown_NG {
	WebDriver driver ;
	
	@BeforeTest
	  public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("chrome");
	  }
	
	
	
  @Test
  public void testDropDown() {
	  
	  //    file:///C:/ZVA/CPSAT/maven_ATA_Ref_ORG/src/test/resources/docs/dropdown.html
	  driver.get("file:///C:/ZVA/CPSAT/maven_ATA_Ref_ORG/src/test/resources/docs/dropdown.html");
	  
	  //       /html/body/form/select    xpath of DropDown
	  By byvar = By.xpath("/html/body/form/select");
	  WebElement weselect = driver.findElement(byvar);
	  
	  Select selobj = new Select(weselect);
	  List<WebElement> lstOptions = selobj.getOptions();
	  
	  
	  // step 9.2
	  for( WebElement element    :  lstOptions) {
		  System.out.println(element.getText());  
		  
	  }
	  try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
	  // Step 9.3
	  
	  //selobj.selectByValue("61");
	  selobj.selectByVisibleText("CP-DOF");
	  
	  try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
	  selobj.selectByIndex(1);
	  
	  
  }
  

  @AfterTest
  public void afterTest() {
	  try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
	  driver.quit();
  }

}
