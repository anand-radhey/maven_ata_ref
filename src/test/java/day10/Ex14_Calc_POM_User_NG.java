package day10;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Ex14_Calc_POM_User_NG {
	WebDriver driver;
	
	@BeforeTest
	  public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("chrome");
	  }
  @Test
  public void f() {
	  driver.get("http://ata123456789123456789.appspot.com/");
	  
	  Ex14_Calc_POM calcPage = new Ex14_Calc_POM(driver);
	  
		int in1 = 12;
		int in2 = 15;
		int expRes = 180;
	  
	  int actRes = calcPage.multiply(in1, in2);
	  System.out.println("Result Value = "+ actRes);
	  Assert.assertEquals(actRes, expRes);
	  
	  int expRes2 = 41;
	  int actRes2 = calcPage.add(10,  30);
	  System.out.println("Result Value for Addition = "+ actRes2);
	  Assert.assertEquals(actRes2, expRes2);
	  
	  
	  
  }
  

  @AfterTest
  public void afterTest() {
	  try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  driver.quit();
  }

}
