package day10;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Ex14_CalcDemo {
	WebDriver driver;

	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("chrome");
	}
	@Test
	public void testMul() {
		driver.get("http://ata123456789123456789.appspot.com/");
		//*[@id="ID_nameField1"]       Field1
		//*[@id="ID_nameField2"]       Field2
		//*[@id="gwt-uid-2"]           Mul
		//*[@id="ID_calculator"]       Calc
		//*[@id="ID_nameField3"]       Result
		
		
		By byField1 = By.xpath("//*[@id=\"ID_nameField1\"]");
		WebElement weField1 = driver.findElement(byField1 );
		
		By byField2 = By.xpath("//*[@id=\"ID_nameField2\"] ");
		WebElement weField2 = driver.findElement(byField2 );
		
		
		By byMul = By.xpath("//*[@id=\"gwt-uid-2\"]");
		WebElement weMul = driver.findElement(byMul );
		
		By byCalc = By.xpath("//*[@id=\"ID_calculator\"]");
		WebElement weCalc = driver.findElement(byCalc );
		
		By byResult = By.xpath("//*[@id=\"ID_nameField3\"]");
		WebElement weResult = driver.findElement( byResult);
		
		//  10 15   150 mul
		// 12   16   add 28
		int in1 = 12;
		int in2 = 15;
		int expRes = 180;
		String sin1 = String.valueOf(in1);
		String sin2 = String.valueOf(in2);
		
		weField1.clear();
		weField1.sendKeys(sin1);
		
		weField2.clear();
		weField2.sendKeys(sin2);
		
		weMul.click();
		
		weCalc.click();
		
		String sResult = weResult.getAttribute("value");
		int iResult = Integer.parseInt(sResult);
		System.out.println(iResult);
		/*
		if(iResult == expRes) {
			System.out.println("Test Passed");
		} else {
			System.out.println("Test Failed");
		}
		*/
		Assert.assertEquals(iResult, expRes, "Anand :: Actual Result is NOT matching with Expected Result");
		
		
		
		
		
		
		
		

	}


	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
