package day10;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class Ex14_Calc_POM {
	WebDriver driver;
	//*[@id="ID_nameField1"]       Field1
	//*[@id="ID_nameField2"]       Field2
	//*[@id="gwt-uid-1"]           Add
	//*[@id="gwt-uid-2"]           Mul
	//*[@id="ID_calculator"]       Calc
	//*[@id="ID_nameField3"]       Result

	By byField1 = By.xpath("//*[@id=\"ID_nameField1\"]");
	By byField2 = By.xpath("//*[@id=\"ID_nameField2\"] ");
	By byAdd = By.xpath("//*[@id=\"gwt-uid-1\"]");
	By byMul = By.xpath("//*[@id=\"gwt-uid-2\"]");
	By byCalc = By.xpath("//*[@id=\"ID_calculator\"]");
	By byResult = By.xpath("//*[@id=\"ID_nameField3\"]");


	public Ex14_Calc_POM(WebDriver driver) {
		this.driver = driver;
	}

	public int multiply(int num1, int num2) {

		WebElement weField1 = driver.findElement(byField1 );
		WebElement weField2 = driver.findElement(byField2 );
		WebElement weMul = driver.findElement(byMul );
		WebElement weCalc = driver.findElement(byCalc );
		WebElement weResult = driver.findElement( byResult);

		String sin1 = String.valueOf(num1);
		String sin2 = String.valueOf(num2);

		weField1.clear();
		weField1.sendKeys(sin1);

		weField2.clear();
		weField2.sendKeys(sin2);

		weMul.click();

		weCalc.click();

		String sResult = weResult.getAttribute("value");
		int iResult = Integer.parseInt(sResult);
		//System.out.println(iResult);

		return iResult;
	}
	
	public int add(int num1, int num2) {
		WebElement weField1 = driver.findElement(byField1 );
		WebElement weField2 = driver.findElement(byField2 );
		WebElement weAdd = driver.findElement(byAdd );
		WebElement weCalc = driver.findElement(byCalc );
		WebElement weResult = driver.findElement( byResult);

		String sin1 = String.valueOf(num1);
		String sin2 = String.valueOf(num2);

		weField1.clear();
		weField1.sendKeys(sin1);

		weField2.clear();
		weField2.sendKeys(sin2);

		weAdd.click();

		weCalc.click();

		String sResult = weResult.getAttribute("value");
		int iResult = Integer.parseInt(sResult);
		//System.out.println(iResult);

		return iResult;
		
	}
	
	
	
}
