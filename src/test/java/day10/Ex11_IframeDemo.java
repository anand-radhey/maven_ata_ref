package day10;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class Ex11_IframeDemo {
	WebDriver driver ;

	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("chrome", true);
	}

	@Test
	public void testIframes() {
		driver.get("https://jqueryui.com/autocomplete/");
		//*[@id="tags"]      xpath of textbox
		
		driver.switchTo().frame(0);
		
		By byvar = By.xpath("//*[@id='tags']");
		WebElement webox = driver.findElement(byvar);
		
		webox.sendKeys("J");
		
		//*[@class='ui-menu-item']   xpath for List items
		
		By byvar2 = By.xpath("//*[@class='ui-menu-item']");
		List<WebElement> lstOptions = driver.findElements(byvar2);
		
		for(WebElement el :    lstOptions) {
			String str = el.getText();
		   System.out.println(str);	
		  
		}
	
		
		for(WebElement el :    lstOptions) {
			String str = el.getText();
		   //System.out.println(str);	
		   if(str.equalsIgnoreCase("java")) {
			   System.out.println("Found Java and clicking ");
			   el.click();
			   break;
		   }
		}
	
	}

	@AfterTest
	public void afterTest() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.quit();
	}

}
