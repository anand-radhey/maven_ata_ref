package day03;

public class Palindrome {

	public static void main(String[] args) {
		
		String str = "mbdam";

		int count = 0;
		for (int i = 0, j = str.length()-1 ; i < str.length() && j >= 0 ; i++, j-- ) {
			System.out.println("i = " + i + "    and j = "+ j);
			if( str.charAt(i) == str.charAt(j)) {
				count++;	
			} 
		}
		if ( count == str.length() ) {
			System.out.println(str + " is a Palindrome");
		} else {
			System.out.println(str + " is NOT a Palindrome");
		}
	}
}


