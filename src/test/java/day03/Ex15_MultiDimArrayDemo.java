package day03;

public class Ex15_MultiDimArrayDemo {

   /*
    *  Q1	    Q2	    Q3	     Q4
2013	1000	5000	6000	3000
2014	2000	6000	3000	7500
2015	2100	2700	5000	1870
	*/
	public static void main(String[] args) {
		/*
		 * int[]  yr1 = { 1000, 5000, 6000, 3000   };
		int[]  yr2 = { 2000,	6000,	3000,	7500  };
		int[]  yr3 = {2100,	2700,	5000,	1870};
		
		int[][]  salesdata = {  yr1, 
								yr2, 
								yr3  };
								*/
		
		
		int[][] anotherSalesdata = 
		
		{
			{ 1000, 5000, 6000, 3000   },
			{ 2000,	6000,	3000,	7500  },
			{2100,	2700,	5000,	1870}
		};
		
		int nrows = anotherSalesdata.length;
		int ncols = anotherSalesdata[0].length;
		
		System.out.println("No. of Rows : " + nrows); //No. of rows
		System.out.println("No. of Columns : " + ncols); // No. of columns
		/*
		for(int i = 0 ; i < nrows ; i++) {
			for(int j=0; j < ncols; j++) {
				System.out.println(i+" , "+j + "    :    "+ anotherSalesdata[i][j]);
			}
		}*/
		
		System.out.println("Q1\tQ2 \t Q3 \t Q4");
		for(int i = 0 ; i < nrows ; i++) {
			int sum = 0;
			for(int j=0; j < ncols; j++) {
				System.out.print( anotherSalesdata[i][j] + "\t" );
				sum = sum + anotherSalesdata[i][j];
			}
			System.out.println(sum);
		}
		

	}

}
