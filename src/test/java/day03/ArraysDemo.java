package day03;

public class ArraysDemo {

	public static void main(String[] args) {
		int x1 = 100;
		int x2 = 200;
		
		String str = "India";
		str.length();
		
		int[]   arrint = { 100, 200, 400, 200, 120, 480, 300, 500 }; 
		
		int len = arrint.length;
		System.out.println(len);
		for(int i = 0; i < len; i++) {
			System.out.println(i + "  value of element = " + arrint[i]);
			
		}
		
		for(int j = 0 ; j < len; j++) {
			arrint[j] = arrint[j] + 10;
		}
		for(int i = 0; i < len; i++) {
			System.out.println(i + "  updated value of element = " + arrint[i]);
			
		}

	}

}
