package day03;

public class Palindrome2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = "Madam";
		
		StringBuffer sb = new StringBuffer(str);
		StringBuffer revsb = sb.reverse();
		String revstr = revsb.toString();
		
		if ( str.equalsIgnoreCase(revstr) ) {
			System.out.println(str + " is a Palindrome");
		} else {
			System.out.println(str + " is NOT a Palindrome");
		}

	}

}
