package day03;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

public class ReadingCSVFile {
	// Read from a given CSV file and print it on console

	public static void main(String[] args) {
		String csvFilePath = "C:\\ZVA\\CPSAT\\maven_ATA_Ref_ORG\\src\\test\\resources\\data\\Cricket_Score.csv";
		 
		CSVReader reader = null;
		
		
		try {
			FileReader fr = new FileReader(csvFilePath);
			
			reader = new CSVReader(fr , ',');
			String[] line;

			while(  (line = reader.readNext())   != null    ) {
				System.out.println(line[0] +"\t"+  line[1] + "\t" + line[2] );
				
			}
	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
